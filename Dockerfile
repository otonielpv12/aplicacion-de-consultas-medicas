FROM node:18 AS ui-build
WORKDIR /usr/src/app
COPY client-app/ ./client-app/
RUN cd client-app && npm install && npm run build

FROM node:18 AS server-build
WORKDIR /root/
COPY --from=ui-build /usr/src/app/client-app/dist ./client-app/dist
COPY package*.json ./api/
ENV STATIC_FILES_PATH="/root/client-app/dist/"
ENV DB_TFG_PASS="OtonielTFG2023"
ENV DB_TFG_HOST="host.docker.internal"
ENV ENVIRONMENT="Production"
RUN cd api && npm install
COPY Controllers ./api/Controllers
COPY Features ./api/Features
COPY Infrastructure ./api/Infrastructure
COPY app.js ./api/


EXPOSE 3080

CMD ["node", "./api/app.js"]