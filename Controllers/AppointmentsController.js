const GetAppointmentsQuery = require("../Features/AppointmentsManagement/Queries/GetAppointmentsQuery");
const CreateNewAppointmentCommand = require("../Features/AppointmentsManagement/Commands/CreateNewAppointmentCommand");
const Mediator = require("../Features/Mediator");
let express = require("express");
const router = express.Router();
const mediator = new Mediator();

router.route("/")
.post(async function (request, response) {

    var body = request.body;
    var command = new CreateNewAppointmentCommand(body.patientId, body.doctorId, body.type, body.date, body.telephoneNumber);

    try {
        var results = await mediator.Execute(command);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
})
.get(async function (request, response) {

    var userId = request.query.userId;
    var isDoctor = request.query.isDoctor || false;
    var currentDate = request.query.currentDate ? new Date(request.query.currentDate) : null;
    var query = new GetAppointmentsQuery(userId, isDoctor, currentDate);

    try {
        var results = await mediator.Execute(query);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
});

module.exports = router;