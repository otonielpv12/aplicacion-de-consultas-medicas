const Mediator = require("../Features/Mediator");
let express = require("express");
const UserLoginCommand = require("../Features/Authentication/Commands/UserLoginCommand");
const router = express.Router();
const mediator = new Mediator();

router.route("/")
.post(async function (request, response) {

    var body = request.body
    var userLoginCommand = new UserLoginCommand(body.password, body.email);

    try {
        var result = await mediator.Execute(userLoginCommand);
        response.status(200).send(result);
    } catch(exception) {
        response.status(401).send({message: exception.message});
    }
    
});

module.exports = router;
