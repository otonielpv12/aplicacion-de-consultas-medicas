const Mediator = require("../Features/Mediator");
let express = require("express");
const GetDoctorsQuery = require("../Features/DoctorsManagement/Queries/GetDoctorsQuery");
const GetDoctorsSpecialitiesQuery = require("../Features/DoctorsManagement/Queries/GetDoctorsSpecialitiesQuery");
const GetDoctorAppointmentsDatesQuery = require("../Features/DoctorsManagement/Queries/GetDoctorAppointmentsDatesQuery");
const router = express.Router();
const mediator = new Mediator();

router.route("/:doctorId/appointments")
.get(async function (request, response) {

    var doctorId = request.params.doctorId;
    var currentDate = request.query.currentDate ? new Date(request.query.currentDate) : null;
    var doctorsQuery = new GetDoctorAppointmentsDatesQuery(doctorId, currentDate);

    try {
        var results = await mediator.Execute(doctorsQuery);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
})

router.route("/specialities")
.get(async function (request, response) {

    var doctorsQuery = new GetDoctorsSpecialitiesQuery();

    try {
        var results = await mediator.Execute(doctorsQuery);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
})

router.route("/")
.get(async function (request, response) {

    var limit = request.query.limit || "";
    var doctorsQuery = new GetDoctorsQuery(limit);

    try {
        var results = await mediator.Execute(doctorsQuery);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
})

module.exports = router;
