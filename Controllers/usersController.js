const Mediator = require("../Features/Mediator");
let express = require("express");
const multer = require("multer");
var path = require('path');
const CreateUserCommand = require("../Features/UsersManagement/Commands/CreateUserCommand");
const GetUserInfoQuery = require("../Features/UsersManagement/Queries/GetUserInfoQuery");
const UpdateUserPasswordCommand = require("../Features/UsersManagement/Commands/UpdateUserPasswordCommand");
const UpdateUserImageCommand = require("../Features/UsersManagement/Commands/UpdateUserImageCommand");
const router = express.Router();
const mediator = new Mediator();

var storage = multer.diskStorage({
    destination: function (request, file, cb) {
        var uploadsFolder = process.env.STATIC_FILES_PATH ? process.env.STATIC_FILES_PATH + "/uploads" : "./client-app/public/uploads"
      cb(null, uploadsFolder)
    },
    filename: function (request, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending .jpg
    }
  })

const upload = multer({
    storage: storage
})

router.route("/:userId/image")
.post(upload.single('file'), async function (request, response) {
    var fileName = request.file ? request.file.filename : null;
    var userId = request.params.userId;
    var updateUserImageCommand = new UpdateUserImageCommand(userId, fileName);

    try {
        var results = await mediator.Execute(updateUserImageCommand);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
});

router.route("/:userId")
.get(async function (request, response) {

    var userId = request.params.userId;
    var getUserInfoQuery = new GetUserInfoQuery(userId);

    try {
        var results = await mediator.Execute(getUserInfoQuery);
        response.status(200).send(results);
    } catch(exception) {
        response.status(404).send({message: exception.message});
    }
})
.post(async function (request, response) {

    var userId = request.params.userId;
    var body = request.body;
    var userCommand = new UpdateUserPasswordCommand(userId, body.currentPassword, body.newPassword);

    try {
        var results = await mediator.Execute(userCommand);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
});

router.route("/")
.post(async function (request, response) {

    var body = request.body
    var userCommand = new CreateUserCommand(body.name, body.lastname, body.password, body.email, body.telephone, body.birthdate);

    try {
        var results = await mediator.Execute(userCommand);
        response.status(200).send(results);
    } catch(exception) {
        if (exception.message.includes("mayor de edad")) {
            response.status(403).send({message: exception.message});
        } else {
            response.status(409).send({message: exception.message});
        }
    }
});

module.exports = router;
