const CloseConsultationCommand = require("../Features/ConsultationsManagement/Commands/CloseConsultationCommand");
const CreateNewConsultationCommand = require("../Features/ConsultationsManagement/Commands/CreateNewConsultationCommand");
const InsertConsultationMessageCommand = require("../Features/ConsultationsManagement/Commands/InsertConsultationMessageCommand");
const GetConsultationsQuery = require("../Features/ConsultationsManagement/Queries/GetConsultationsQuery");
const Mediator = require("../Features/Mediator");
let express = require("express");
const router = express.Router();
const mediator = new Mediator();

router.route("/:consultationId/close")
.put(async function (request, response) {

    var consultationId = request.params.consultationId;
    var command = new CloseConsultationCommand(consultationId);

    try {
        var results = await mediator.Execute(command);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
});

router.route("/:consultationId")
.put(async function (request, response) {

    var consultationId = request.params.consultationId;
    var body = request.body;
    var command = new InsertConsultationMessageCommand(consultationId, body.userId, body.message);

    try {
        var results = await mediator.Execute(command);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
});

router.route("/")
.post(async function (request, response) {

    var body = request.body
    var command = new CreateNewConsultationCommand(body.patientId, body.doctorId, body.title, body.message);

    try {
        var results = await mediator.Execute(command);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
})
.get(async function (request, response) {

    var userId = request.query.userId;
    var isDoctor = request.query.isDoctor || false;
    var query = new GetConsultationsQuery(userId, isDoctor);

    try {
        var results = await mediator.Execute(query);
        response.status(200).send(results);
    } catch(exception) {
        response.status(409).send({message: exception.message});
    }
});


module.exports = router;
