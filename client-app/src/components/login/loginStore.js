import { defineStore } from "pinia";
import router from "../../router";
import { getActivePinia } from "pinia";
import { CometChat } from "@cometchat-pro/chat";

const USER_BASIC_TYPE = "patient";

export const useLoginStore = defineStore({
    id: "loginStore",
    state: () => ({
        error: "",
        loading: false,
        localStorageKey: "USER",
        lastPathTo: "/profile",
        userInfo: {
            id: "",
            user: "",
            pass: "",
            token: "",
            type: USER_BASIC_TYPE
        }
    }),
    actions: {
        getUserLogin() {
            this.error = "";
            this.userInfo.token = "";

            const loginInfo = {
                email: this.userInfo.user,
                password: this.userInfo.pass
            };

            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(loginInfo)
            };

            fetch("/api/login/", requestOptions)
                .then(async response => {
                    const data = await response.json();

                    if (!response.ok) {
                        this.error = (data && data.message) || response.statusText;
                    }
                    else {
                        this.userInfo.id = data.userId;
                        this.userInfo.pass = "";
                        this.userInfo.token = data.token;
                        this.userInfo.type = data.type;
                        var apiKey = process.env.VUE_APP_COMMETCHAT_API_KEY;
                        await CometChat.login(this.userInfo.id, apiKey);
                    }
                });
        },
        logout() {
            var apiKey = process.env.VUE_APP_COMMETCHAT_API_KEY;
            CometChat.logout(this.userInfo.id, apiKey);

            this.userInfo.user = "";
            this.userInfo.pass = "";
            this.userInfo.token = "";
            this.userInfo.type = USER_BASIC_TYPE;

            window.localStorage.removeItem(this.localStorageKey);
            window.localStorage.removeItem("SELECTED_CONSULTATION_ID");

            getActivePinia()._s.forEach(store => store.$reset());

            router.push({ name: "home" });
        },
        saveUserInfo() {
            window.localStorage.setItem(this.localStorageKey, JSON.stringify(this.userInfo));
            router.push({ name: "profile" });
        },
        updateLastPathTo(lastPathTo) {
            if (lastPathTo.indexOf("login") == -1) {
                this.lastPathTo = lastPathTo;
            }
        },
        parseJwt(token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

            return JSON.parse(jsonPayload);
        },
        checkJwtExpiration() {
            const localUserInfo = window.localStorage.getItem(this.localStorageKey);
            if (localUserInfo) {
                var token = JSON.parse(localUserInfo).token;
                const payload = this.parseJwt(token);

                if (payload.exp < Date.now() / 1000) {
                    window.localStorage.removeItem(this.localStorageKey);
                }
            }
        }
    }
});