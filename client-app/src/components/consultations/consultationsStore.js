import { defineStore } from "pinia";
import { useLoginStore } from "../login/loginStore";

export const useConsultationsStore = defineStore({
    id: "consultationsStore",
    state: () => ({
        isPageLoading: true,
        error: "",
        consultations: [],
        selectedConsultation: {},
        newMessage: {}
    }),
    actions: {
        async getConsultations() {
            const loginStore = useLoginStore();

            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            var apiUrl = `/api/consultations?userId=${loginStore.userInfo.id}`;

            if (loginStore.userInfo.type == "doctor") {
                apiUrl += `&isDoctor=true`;
            }

            fetch(apiUrl, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.consultations = data;
                }
            });
            
            this.isPageLoading = false;
        },
        async insertConsultationMessage() {

            const requestOptions = {
                method: "PUT",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(this.newMessage)
            };

            fetch(`/api/consultations/${this.selectedConsultation.Id}`, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.newMessage = {};
                }
            });
        },
        async closeConsultation() {

            const requestOptions = {
                method: "PUT",
                headers: { "Content-Type": "application/json" }
            };

            fetch(`/api/consultations/${this.selectedConsultation.Id}/close`, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                } else {
                    window.location.reload();
                }
            });
        }
    }
});