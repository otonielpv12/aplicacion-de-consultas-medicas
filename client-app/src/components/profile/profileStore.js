import { defineStore } from "pinia";
import { useLoginStore } from "../login/loginStore";

export const useProfileStore = defineStore({
    id: "profileStore",
    state: () => ({
        isPageLoading: true,
        error: "",
        passwordSucess: "",
        imageSucess: "",
        userInfo: {
            name: "",
            lastName: "",
            email: "",
            telephone: "",
            birthdate: "",
            image: ""
        }
    }),
    actions: {
        async getUserInfoOnlyOneTime() {
            var userInfoHasInfo = Object.values(this.userInfo).every(value => value != '');
            if (userInfoHasInfo) {
                return;
            }
            const loginStore = useLoginStore();
            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            fetch(`/api/users/${loginStore.userInfo.id}`, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.userInfo.name = data.Name;
                    this.userInfo.lastName = data.Lastname;
                    this.userInfo.email = data.Email;
                    this.userInfo.telephone = data.Telephone;
                    this.userInfo.birthdate = data.Birthdate;
                    this.userInfo.image = data.Image;
                }
            });
            
            this.isPageLoading = false;
        },
        async updateUserPassword(currentPassword, newPassword) {
            const loginStore = useLoginStore();
            const updatePasswordInfo = {
                currentPassword: currentPassword,
                newPassword: newPassword
            }
            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(updatePasswordInfo)
            };

            fetch(`/api/users/${loginStore.userInfo.id}`, requestOptions)
            .then(async response => {
                const data = await response.json() || null;

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.passwordSucess = "Contraseña cambiada con exito";
                }
            });
            
            this.isPageLoading = false;
        }
    }
});