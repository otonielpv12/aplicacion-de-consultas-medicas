import { defineStore } from "pinia";
import router from "../../router";

export const useHomeStore = defineStore({
    id: "homeStore",
    state: () => ({
        isPageLoading: true,
        error: "",
        doctors: [],
        selectedDoctor: {
            id: "",
            fullName: "",
        },
        newConsultationInfo: {
            patientId: "",
            doctorId: "",
            title: "",
            message: "",
        }
    }),
    actions: {
        async getDoctorsOnlyOneTime() {
            var doctorsHasInfo = this.doctors.length > 0;
            if (doctorsHasInfo) {
                return;
            }
            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            fetch(`/api/doctors?limit=15`, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.doctors.push(...data);
                }
            });
            
            this.isPageLoading = false;
        },
        async createConsultation() {
            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(this.newConsultationInfo)
            };

            fetch(`/api/consultations`, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.newConsultationInfo.doctorId = "";
                    this.newConsultationInfo.patientId = "";
                    this.newConsultationInfo.title = "";
                    this.newConsultationInfo.message = "";
                    router.push("/consultations");
                }
            });
        }
    }
});