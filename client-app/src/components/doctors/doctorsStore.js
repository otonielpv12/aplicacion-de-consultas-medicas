import { defineStore } from "pinia";
import router from "../../router";

export const useDoctorsStore = defineStore({
    id: "doctorsStore",
    state: () => ({
        isPageLoading: true,
        error: "",
        doctors: [],
        doctorsSpecialities: [],
        selectedDoctor: {
            id: "",
            fullName: "",
            takenDates: []
        },
        newConsultationInfo: {
            patientId: "",
            doctorId: "",
            title: "",
            message: "",
        },
        newAppointmentInfo: {
            patientId: "",
            doctorId: "",
            type: "",
            date: undefined,
            telephoneNumber: ""
        }
    }),
    actions: {
        async getDoctorsOnlyOneTime() {
            var doctorsHasInfo = this.doctors.length > 0;
            if (doctorsHasInfo) {
                return;
            }
            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            fetch(`/api/doctors`, requestOptions)
                .then(async response => {
                    const data = await response.json();

                    if (!response.ok) {
                        this.error = (data && data.message) || response.statusText;
                    }
                    else {
                        this.doctors.push(...data);
                    }
                });

            this.isPageLoading = false;
        },
        async getDoctorsSpecialitiesOnlyOneTime() {
            var doctorsSpecialitiesHasInfo = this.doctorsSpecialities.length > 0;
            if (doctorsSpecialitiesHasInfo) {
                return;
            }
            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            fetch(`/api/doctors/specialities`, requestOptions)
                .then(async response => {
                    const data = await response.json();

                    if (!response.ok) {
                        this.error = (data && data.message) || response.statusText;
                    }
                    else {
                        this.doctorsSpecialities.push(...data);
                    }
                });

            this.isPageLoading = false;
        },
        async createConsultation() {
            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(this.newConsultationInfo)
            };

            fetch(`/api/consultations`, requestOptions)
                .then(async response => {
                    const data = await response.json();

                    if (!response.ok) {
                        this.error = (data && data.message) || response.statusText;
                    }
                    else {
                        this.newConsultationInfo.doctorId = "";
                        this.newConsultationInfo.patientId = "";
                        this.newConsultationInfo.title = "";
                        this.newConsultationInfo.message = "";
                        router.push("/consultations");
                    }
                });
        },
        async getDoctorsTakenDates() {
            let currentDate = new Date();
            var parsedDate = currentDate.toISOString().split('T')[0];
            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            fetch(`/api/doctors/${this.selectedDoctor.id}/appointments?currentDate=${parsedDate}`, requestOptions)
                .then(async response => {
                    const data = await response.json();

                    if (!response.ok) {
                        this.error = (data && data.message) || response.statusText;
                    }
                    else {
                        this.selectedDoctor.takenDates = data;
                    }
                });

            this.isPageLoading = false;
        },
        async createAppointment() {
            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(this.newAppointmentInfo)
            };

            fetch(`/api/appointments`, requestOptions)
                .then(async response => {
                    const data = await response.json();

                    if (!response.ok) {
                        this.error = (data && data.message) || response.statusText;
                    }
                    else {
                        this.newAppointmentInfo.doctorId = "";
                        this.newAppointmentInfo.patientId = "";
                        this.newAppointmentInfo.type = "";
                        this.newAppointmentInfo.date = undefined;
                        window.location.reload();
                    }
                });
        }
    }
});