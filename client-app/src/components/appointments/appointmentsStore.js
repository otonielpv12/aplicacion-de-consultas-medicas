import { defineStore } from "pinia";
import { useLoginStore } from "../login/loginStore";

export const useAppointmentsStore = defineStore({
    id: "appointmentsStore",
    state: () => ({
        isPageLoading: true,
        error: "",
        appointments: []
    }),
    actions: {
        async getAppointments() {
            const loginStore = useLoginStore();

            const requestOptions = {
                method: "GET",
                headers: { "Content-Type": "application/json" }
            };

            var apiUrl = `/api/appointments?userId=${loginStore.userInfo.id}`;

            if (loginStore.userInfo.type == "doctor") {
                apiUrl += `&isDoctor=true`;
            }
            var currentDate = new Date();
            apiUrl += `&currentDate=${currentDate.toISOString()}`;

            fetch(apiUrl, requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {
                    this.appointments = data;
                }
            });
            
            this.isPageLoading = false;
        }
    }
});