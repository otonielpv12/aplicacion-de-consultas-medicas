import { defineStore } from "pinia";
import { useLoginStore } from "../login/loginStore";
import router from "../../router";

export const useRegisterStore = defineStore({
    id: "registerStore",
    state: () => ({
        error: "",
        userInfo: {
            name: "",
            lastName: "",
            email: "",
            confirmEmail: "",
            pass: "",
            telephone: "",
            birthdate: ""
        }
    }),
    actions: {
        registerUser() {
            const loginStore = useLoginStore();
            const registerInfo = {
                name: this.userInfo.name,
                lastname: this.userInfo.lastName,
                email: this.userInfo.email,
                password: this.userInfo.pass,
                telephone: this.userInfo.telephone,
                birthdate: this.userInfo.birthdate
            };

            const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(registerInfo)
            };

            fetch("/api/users/", requestOptions)
            .then(async response => {
                const data = await response.json();

                if (!response.ok) {
                    this.error = (data && data.message) || response.statusText;
                }
                else {

                    loginStore.userInfo.user = this.userInfo.email;
                    loginStore.userInfo.pass = this.userInfo.pass;
                    
                    this.userInfo.name = "";
                    this.userInfo.lastName = "";
                    this.userInfo.email = "";
                    this.userInfo.confirmEmail = "";
                    this.userInfo.pass = "";
                    this.userInfo.telephone = "";
                    this.userInfo.birthdate = "";

                    loginStore.getUserLogin();
                    router.push("/profile");
                }
            });
        }
    }
});