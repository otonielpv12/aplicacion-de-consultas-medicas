import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import { useLoginStore } from "../components/login/loginStore.js"
import HomeView from '../views/HomeView.vue'
import Login from '../views/Login.vue'
import Register from "../views/Register.vue"
import Profile from '../views/Profile.vue'
import AboutUs from "../views/AboutUs.vue"
import Consultations from "../views/Consultations.vue"
import Doctors from "../views/Doctors.vue"
import Appointments from "../views/Appointments.vue"
import VideoCall from "../views/VideoCall.vue"
import ContactUs from "../views/ContactUs.vue"
import { useProfileStore } from '@/components/profile/profileStore.js'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about-us',
    name: 'about-us',
    component: AboutUs
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/consultations',
    name: 'consultations',
    component: Consultations,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/doctors',
    name: 'doctors',
    component: Doctors,
  },
  {
    path: '/appointments',
    name: 'appointments',
    component: Appointments,
    meta: {
      requiresAuth: true
    },
  },
  {
    path: '/appointments/:id/videocall/:userId',
    name: 'videocall',
    component: VideoCall,
    meta: {
      requiresAuth: true
    },
  },
  
  {
    path: '/contact-us',
    name: 'contact-us',
    component: ContactUs,
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  const loginStore = useLoginStore();
  const profileStore = useProfileStore();
  loginStore.updateLastPathTo(to.path);

  const localStorageUser = window.localStorage.getItem(loginStore.localStorageKey);
  let localUserInfo = {};

  if (localStorageUser) {
    localUserInfo = JSON.parse(localStorageUser);
    loginStore.userInfo = (localUserInfo) ? localUserInfo : {};
    profileStore.getUserInfoOnlyOneTime();
  }

  if (to.path == '/register' && localStorageUser) {
    next({ name: "home" });
  }

  if (to.meta.requiresAuth && (!localUserInfo || !localUserInfo.token)) {
    next({ name: "login" });
  }
  else if (to.name == "login" && (localUserInfo && localUserInfo.token)) {
    next({ name: "home" });
  }
  else {
    next();
  }
});

export default router
