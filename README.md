# Como lanzar la aplicación en local
## Prerequisitos
* Tener instalado node, puede descargarlo [aqui](https://nodejs.org/es) 
* MYSQL + Gestor, actualmente uso XAMP, puede descargarlo [aqui](https://www.apachefriends.org/download.html)
## Pasos para levantar la aplicación

1. Descargar el REPO
2. Aplicar el script SQL que se encuentra dentro de la carpeta mysql-dockerfile
3. Abrir una terminal en la raiz del repo y ejecutar `npm install`
4. Lanzar el comando `nodemon .\app.js` para levantar el **BACK-END**
5. Abrir una terminal en la carpeta client-app del repo y ejecutar `npm install` 
6. Lanzar el comando `npm run serve` para levantar el **FRONT-END**