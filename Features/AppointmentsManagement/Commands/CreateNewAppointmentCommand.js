class CreateNewAppointmentCommand {

    constructor(patientId, doctorId, type, date, telephoneNumber) {
        this.PatientId = patientId;
        this.DoctorId = doctorId;
        this.Type = type;
        this.Date = date;
        this.TelephoneNumber = telephoneNumber || null;
    }
}

module.exports = CreateNewAppointmentCommand;