require('dotenv').config();

class CreateNewAppointmentCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            var date = command.Date;
            if (process.env.ENVIRONMENT == "Production") {
                date = new Date(command.Date);
            }
            let sqlQuery = "INSERT INTO appointments (PatientId, DoctorId, Type, Date, TelephoneNumber) VALUES (?, ?, ?, ?, ?)";
            connectionProvider.query(sqlQuery, 
                [command.PatientId, command.DoctorId, command.Type, date, command.TelephoneNumber],
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        resolve({id: results.insertId});
                    }
                });
        });
    }
}

module.exports = CreateNewAppointmentCommandHandler;