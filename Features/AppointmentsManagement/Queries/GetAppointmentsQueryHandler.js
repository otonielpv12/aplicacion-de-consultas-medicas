class GetAppointmentsQueryHandler {

    static Handle(connectionProvider, query) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "SELECT a.Id, a.PatientId, a.Type, a.Date, CONCAT(u.Name, ' ', u.Lastname) AS OtherFullName, a.TelephoneNumber FROM appointments a JOIN users u";
            if (query.IsDoctor) {
                sqlQuery += ` ON a.PatientId = u.Id WHERE a.DoctorId = ? AND Date BETWEEN '${query.CurrentDate.toISOString()}' AND DATE_ADD('${query.CurrentDate.toISOString()}', INTERVAL 1 WEEK) `;
            } else {
                sqlQuery += ` ON a.DoctorId = u.Id WHERE a.PatientId = ? AND DATE >= '${query.CurrentDate.toISOString()}' `;
            }
            sqlQuery += "ORDER BY a.Date ASC";

            connectionProvider.query(sqlQuery, 
                query.UserId,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results && results.length > 0) {
                            resolve(results);
                        }
                    }
                });
        });
    }
}

module.exports = GetAppointmentsQueryHandler;