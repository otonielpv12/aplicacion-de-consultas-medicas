class GetAppointmentsQuery {

    constructor(userId, isDoctor, currentDate) {
        let newCurrentDate = new Date()
        newCurrentDate.toISOString().split('T')[0]
        this.UserId = userId;
        this.IsDoctor = isDoctor;
        this.CurrentDate = currentDate || newCurrentDate;
    }
}

module.exports = GetAppointmentsQuery;