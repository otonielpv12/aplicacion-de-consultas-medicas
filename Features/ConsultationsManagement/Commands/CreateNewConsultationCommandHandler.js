class CreateNewConsultationCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            var messages = [];
            var newMessage = {
                id: 0,
                userId: parseInt(command.PatientId),
                date: new Date(),
                message: command.Message
            }
            messages.push(newMessage);
            let sqlQuery = "INSERT INTO consultations (PatientId, DoctorId, Title, Messages) VALUES (?, ?, ?, ?)";
            connectionProvider.query(sqlQuery, 
                [command.PatientId, command.DoctorId, command.Title, JSON.stringify(messages)],
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        resolve({id: results.insertId});
                    }
                });
        });
    }
}

module.exports = CreateNewConsultationCommandHandler;