class InsertConsultationMessageCommand {

    constructor(consultationId, userId, message) {
        this.ConsultationId = consultationId;
        this.UserId = userId;
        this.Message = message;
    }
}

module.exports = InsertConsultationMessageCommand;