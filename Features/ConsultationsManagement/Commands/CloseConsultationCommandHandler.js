class CloseConsultationCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "UPDATE consultations SET IsClosed='1' WHERE Id=?"
            connectionProvider.query(sqlQuery, 
                command.ConsultationId,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        resolve({id: results.insertId});
                    }
                });
        });
    }
}

module.exports = CloseConsultationCommandHandler;