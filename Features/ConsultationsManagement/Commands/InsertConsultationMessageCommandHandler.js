class InsertConsultationMessageCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            InsertConsultationMessageCommandHandler.getPreviousMessages(connectionProvider, command.ConsultationId)
            .then(function(results) {
                var messages = results;
                let lastMessageId = Math.max.apply(Math, messages.map(function(o) { return o.id; }));
                var newMessage = {
                    id: lastMessageId + 1,
                    userId: parseInt(command.UserId),
                    date: new Date(),
                    message: command.Message
                }
                messages.push(newMessage);
                let sqlQuery = "UPDATE consultations SET Messages=? WHERE Id=?";
                connectionProvider.query(sqlQuery, 
                    [JSON.stringify(messages), command.ConsultationId],
                    function(error, results)
                    {
                        if (error)
                        {
                            reject(error);
                        }
                        else
                        {
                            resolve({id: results.insertId});
                        }
                    });
            })
            .catch(function(error)
            {
                reject(error);
            })
        });
    }

    static getPreviousMessages(connectionProvider, consultationId) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "SELECT Messages FROM consultations WHERE Id=?";
            connectionProvider.query(sqlQuery, consultationId,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        let responses = (results.length && results.length > 0) ? JSON.parse(results[0].Messages) : [];
                        resolve(responses);
                    }
                });
        });
    }
}

module.exports = InsertConsultationMessageCommandHandler;