class CreateNewConsultationCommand {

    constructor(patientId, doctorId, title, message) {
        this.PatientId = patientId;
        this.DoctorId = doctorId;
        this.Title = title;
        this.Message = message;
    }
}

module.exports = CreateNewConsultationCommand;