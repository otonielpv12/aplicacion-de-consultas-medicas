class GetConsultationsQueryHandler {

    static Handle(connectionProvider, query) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "SELECT c.Id, c.PatientId, c.Title, c.Messages, c.IsClosed, CONCAT(u.Name, ' ', u.Lastname) AS ReceptorsFullName FROM consultations c JOIN users u";
            if (query.IsDoctor) {
                sqlQuery += " ON c.PatientId = u.Id WHERE c.DoctorId = ? ";
            } else {
                sqlQuery += " ON c.DoctorId = u.Id WHERE c.PatientId = ? ";
            }
            sqlQuery += "ORDER BY c.IsClosed ASC, c.Id DESC";

            connectionProvider.query(sqlQuery, 
                query.UserId,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results && results.length > 0) {
                            resolve(results);
                        }
                    }
                });
        });
    }
}

module.exports = GetConsultationsQueryHandler;