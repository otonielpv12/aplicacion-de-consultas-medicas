class GetConsultationsQuery {

    constructor(userId, isDoctor) {
        this.UserId = userId;
        this.IsDoctor = isDoctor;
    }
}

module.exports = GetConsultationsQuery;