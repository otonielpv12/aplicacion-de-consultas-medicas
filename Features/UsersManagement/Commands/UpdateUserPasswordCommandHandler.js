const md5 = require('md5');

class UpdateUserPasswordCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            let currentEncryptedPassword = md5(command.CurrentPassword);
            let newEncryptedPassword = md5(command.NewPassword);
            let sqlQuery = "UPDATE users SET Password=? WHERE Id=? AND Password=?";
            connectionProvider.query(sqlQuery, 
                [newEncryptedPassword, command.UserId, currentEncryptedPassword],
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results.affectedRows == 0) reject(new Error("Contraseña actual incorrecta"));
                        resolve(results);
                    }
                });
        });
    }
}

module.exports = UpdateUserPasswordCommandHandler;