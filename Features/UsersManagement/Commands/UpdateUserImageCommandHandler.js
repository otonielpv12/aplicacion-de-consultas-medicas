class UpdateUserImageCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "UPDATE users SET Image=? WHERE Id=?";
            connectionProvider.query(sqlQuery, 
                [command.ImageName, command.UserId],
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        resolve(results);
                    }
                });
        });
    }
}

module.exports = UpdateUserImageCommandHandler;