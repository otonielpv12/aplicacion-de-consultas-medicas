class UpdateUserPasswordCommand {

    constructor(userId, currentPassword, newPassword) {
        this.UserId = userId;
        this.CurrentPassword = currentPassword;
        this.NewPassword = newPassword;
    }
}

module.exports = UpdateUserPasswordCommand;