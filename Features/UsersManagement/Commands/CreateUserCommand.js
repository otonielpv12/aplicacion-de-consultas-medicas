class CreateUserCommand {

    constructor(name, lastname, password, email, telephone, birthdate) {
        this.Name = name;
        this.Lastname = lastname
        this.Password = password;
        this.Email = email;
        this.Telephone = telephone;
        this.Birthdate = birthdate;
    }
}

module.exports = CreateUserCommand;