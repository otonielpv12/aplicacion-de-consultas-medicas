class UpdateUserImageCommand {

    constructor(userId, imageName) {
        this.UserId = userId;
        this.ImageName = imageName;
    }
}

module.exports = UpdateUserImageCommand;