const md5 = require('md5');

class CreateUserCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            var parsedBirthdate = new Date(command.Birthdate);
            var nowDate = new Date();
            var diffInMiliseconds = nowDate - parsedBirthdate;
            var diffInYears = diffInMiliseconds / (1000 * 60 * 60 * 24 * 365);
            if (diffInYears < 18) {
                reject(new Error("Debes ser mayor de edad para registrarte"));
                return;
            }
            let encryptedPassword = md5(command.Password);
            let sqlQuery = "INSERT INTO users (Name, Lastname, Password, Email, Telephone, Birthdate) VALUES (?, ?, ?, ?, ?, ?)";
            connectionProvider.query(sqlQuery, 
                [command.Name, command.Lastname, encryptedPassword, command.Email, command.Telephone, parsedBirthdate],
                function(error, results)
                {
                    if (error)
                    {
                        if (error.sqlMessage == `Duplicate entry '${command.Email}' for key 'Email'`) {
                            reject(new Error("Correo electronico en uso"))
                        }
                        reject(error);
                    }
                    else
                    {
                        resolve({id: results.insertId});
                    }
                });
        });
    }
}

module.exports = CreateUserCommandHandler;