class GetUserInfoQueryHandler {

    static Handle(connectionProvider, query) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "SELECT Name, Lastname, Email, Telephone, Birthdate, Image FROM users WHERE Id = ?";
            connectionProvider.query(sqlQuery, 
                [query.Id],
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results && results.length > 0) {
                            results[0].Birthdate = results[0].Birthdate.toISOString().slice(0,10)
                            resolve(results[0]);
                        }
                        reject(new Error("El usuario no está registrado"));
                    }
                });
        });
    }
}

module.exports = GetUserInfoQueryHandler;