const md5 = require('md5');
const jwt = require('jsonwebtoken');

class UserLoginCommandHandler {

    static Handle(connectionProvider, command) {
        return new Promise(function(resolve, reject)
        {
            let encryptedPassword = md5(command.Password);
            let sqlQuery = "SELECT Id, Type FROM users WHERE Password=? AND Email=?";
            connectionProvider.query(sqlQuery, 
                [encryptedPassword, command.Email],
                function(error, results)
                {
                    if (error)
                    {
                        
                        reject(error);
                    }
                    else
                    {
                        let loggedUser;
                        if (results && results.length > 0) {
                            loggedUser = getValidUserInfo(results);
                        } else {
                            reject(new Error("Usuario o contraseña invalida"));
                        }
                        resolve(loggedUser);
                    }
                });
        });
    };
}

function getValidUserInfo(userInfo)
{
    const EXP_TIME_IN_SECS = 14400;
    const TOKEN_SECRET = "8F8C7BAD1ECF2CF954F2C19D2DD8E";

    let loggedUserInfo = {
        userId: userInfo[0].Id,
        userType: userInfo[0].Type
    }

    let validUserInfo = {
        token: jwt.sign(loggedUserInfo, TOKEN_SECRET, { expiresIn: EXP_TIME_IN_SECS }),
        type: userInfo[0].Type,
        userId: userInfo[0].Id
    };

    return validUserInfo;
}

module.exports = UserLoginCommandHandler;