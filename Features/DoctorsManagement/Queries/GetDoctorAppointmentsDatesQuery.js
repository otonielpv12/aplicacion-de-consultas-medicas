class GetDoctorAppointmentsDatesQuery {

    constructor(userId, currentDate) {
        this.UserId = userId;
        this.CurrentDate = currentDate;
    }
}

module.exports = GetDoctorAppointmentsDatesQuery;