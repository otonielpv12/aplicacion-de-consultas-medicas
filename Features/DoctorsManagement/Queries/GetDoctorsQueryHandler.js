class GetDoctorsQueryHandler {

    static Handle(connectionProvider, query) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "SELECT Id, CONCAT(Name, ' ', Lastname) AS DoctorsFullName, Email, Telephone, Speciality, Image FROM users WHERE Type = 'doctor'";
            if (query.Limit) {
                sqlQuery += ` LIMIT ${query.Limit}`
            }
            connectionProvider.query(sqlQuery,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results && results.length > 0) {
                            resolve(results);
                        }
                    }
                });
        });
    }
}

module.exports = GetDoctorsQueryHandler;