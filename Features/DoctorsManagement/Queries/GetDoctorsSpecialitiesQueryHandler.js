class GetDoctorsSpecialitiesQueryHandler {

    static Handle(connectionProvider, query) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = "SELECT Speciality, COUNT(*) AS Count FROM users WHERE Speciality IS NOT NULL GROUP BY Speciality ORDER BY Speciality ASC";
            connectionProvider.query(sqlQuery,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results && results.length > 0) {
                            resolve(results);
                        }
                    }
                });
        });
    }
}

module.exports = GetDoctorsSpecialitiesQueryHandler;