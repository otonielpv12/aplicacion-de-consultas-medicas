class GetDoctorAppointmentsDatesQueryHandler {

    static Handle(connectionProvider, query) {
        return new Promise(function(resolve, reject)
        {
            let sqlQuery = `SELECT Date FROM appointments WHERE DoctorId = ? AND DATE >= '${query.CurrentDate.toISOString()}' ORDER BY Id ASC`;

            connectionProvider.query(sqlQuery, 
                query.UserId,
                function(error, results)
                {
                    if (error)
                    {
                        reject(error);
                    }
                    else
                    {
                        if (results && results.length > 0) {
                            resolve(results);
                        } else {
                            resolve([]);
                        }
                    }
                });
        });
    }
}

module.exports = GetDoctorAppointmentsDatesQueryHandler;