const ConnectionProvider = require("../Infrastructure/ConnectionProvider");
const UserLoginCommandHandler = require("./Authentication/Commands/UserLoginCommandHandler");
const GetUserInfoQueryHandler = require("./UsersManagement/Queries/GetUserInfoQueryHandler");
const CreateUserCommandHandler = require("./UsersManagement/Commands/CreateUserCommandHandler");
const UpdateUserPasswordCommandHandler = require("./UsersManagement/Commands/UpdateUserPasswordCommandHandler");
const GetDoctorsQueryHandler = require("./DoctorsManagement/Queries/GetDoctorsQueryHandler");
const UpdateUserImageCommandHandler = require("./UsersManagement/Commands/UpdateUserImageCommandHandler");
const CreateNewConsultationCommandHandler = require("./ConsultationsManagement/Commands/CreateNewConsultationCommandHandler");
const InsertConsultationMessageCommandHandler = require("./ConsultationsManagement/Commands/InsertConsultationMessageCommandHandler");
const GetConsultationsQueryHandler = require("./ConsultationsManagement/Queries/GetConsultationsQueryHandler");
const CloseConsultationCommandHandler = require("./ConsultationsManagement/Commands/CloseConsultationCommandHandler");
const GetDoctorsSpecialitiesQueryHandler = require("./DoctorsManagement/Queries/GetDoctorsSpecialitiesQueryHandler");
const GetAppointmentsQueryHandler = require("./AppointmentsManagement/Queries/GetAppointmentsQueryHandler");
const CreateNewAppointmentCommandHandler = require("./AppointmentsManagement/Commands/CreateNewAppointmentCommandHandler");
const GetDoctorAppointmentsDatesQueryHandler = require("./DoctorsManagement/Queries/GetDoctorAppointmentsDatesQueryHandler");

class Mediator {

    constructor() {
        const connectionProvider = new ConnectionProvider();
        this.sqlconnection = connectionProvider.getConnection(false);
    }

    Execute(request) {
        switch (request.constructor.name) {
            case "CreateUserCommand":
                return CreateUserCommandHandler.Handle(this.sqlconnection, request);
            case "GetUserInfoQuery":
                return GetUserInfoQueryHandler.Handle(this.sqlconnection, request);
            case "UserLoginCommand":
                return UserLoginCommandHandler.Handle(this.sqlconnection, request);
            case "UpdateUserPasswordCommand":
                return UpdateUserPasswordCommandHandler.Handle(this.sqlconnection, request);
            case "UpdateUserImageCommand":
                return UpdateUserImageCommandHandler.Handle(this.sqlconnection, request);
            case "GetDoctorsQuery":
                return GetDoctorsQueryHandler.Handle(this.sqlconnection, request);
            case "CreateNewConsultationCommand":
                return CreateNewConsultationCommandHandler.Handle(this.sqlconnection, request);
            case "InsertConsultationMessageCommand":
                return InsertConsultationMessageCommandHandler.Handle(this.sqlconnection, request);
            case "GetConsultationsQuery":
                return GetConsultationsQueryHandler.Handle(this.sqlconnection, request);
            case "CloseConsultationCommand":
                return CloseConsultationCommandHandler.Handle(this.sqlconnection, request);
            case "GetDoctorsSpecialitiesQuery":
                return GetDoctorsSpecialitiesQueryHandler.Handle(this.sqlconnection, request);
            case "GetAppointmentsQuery":
                return GetAppointmentsQueryHandler.Handle(this.sqlconnection, request);
            case "CreateNewAppointmentCommand":
                return CreateNewAppointmentCommandHandler.Handle(this.sqlconnection, request);
            case "GetDoctorAppointmentsDatesQuery":
                return GetDoctorAppointmentsDatesQueryHandler.Handle(this.sqlconnection, request);

            default:
                break;
        }
    }
}

module.exports = Mediator;