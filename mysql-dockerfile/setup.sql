-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-05-2023 a las 20:48:52
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `consultas_medicas_dev`
--
CREATE DATABASE consultas_medicas_dev;

USE consultas_medicas_dev;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appointments`
--

CREATE TABLE `appointments` (
  `Id` int(11) NOT NULL,
  `PatientId` int(255) NOT NULL,
  `DoctorId` int(255) NOT NULL,
  `Type` varchar(256) NOT NULL,
  `Date` datetime NOT NULL,
  `TelephoneNumber` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultations`
--

CREATE TABLE `consultations` (
  `Id` int(255) NOT NULL,
  `PatientId` int(255) NOT NULL,
  `DoctorId` int(255) NOT NULL,
  `Title` varchar(256) NOT NULL,
  `Messages` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`Messages`)),
  `IsClosed` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `Id` int(255) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `Lastname` varchar(256) NOT NULL,
  `Password` varchar(256) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `Telephone` varchar(256) NOT NULL,
  `Birthdate` date NOT NULL,
  `Type` varchar(256) NOT NULL DEFAULT 'patient',
  `ShortDescription` varchar(256) DEFAULT NULL,
  `Speciality` varchar(256) DEFAULT NULL,
  `Image` varchar(256) DEFAULT 'profile-icon.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`Id`, `Name`, `Lastname`, `Password`, `Email`, `Telephone`, `Birthdate`, `Type`, `ShortDescription`, `Speciality`, `Image`) VALUES
(1, 'Dr. Alejandro', 'Navarro', '42f749ade7f9e195bf475f37a44cafcb', 'anavarro@healthnow.com', '123-456-789', '1989-07-18', 'doctor', NULL, 'Cirugía General', '1684534988248.jpg'),
(2, 'Dra. Camila', 'Sánchez', 'aa6860ecf06be6c60f631705c58e0b11', 'csanchez@healthnow.com', '987-654-321', '1996-01-12', 'doctor', NULL, 'Pediatría', 'profile-icon.png'),
(3, 'Dr. Gabriel', 'Morales', '68e109f0f40ca72a15e05cc22786f8e6', 'gmorales@healthnow.com', '234-567-890', '1982-12-28', 'doctor', NULL, 'Ginecología y Obstetricia', 'profile-icon.png'),
(4, 'Dra. Lucía', 'Mendoza', '6ebe76c9fb411be97b3b0d48b791a7c9', 'lmendoza@healthnow.com', '876-543-210', '1990-04-07', 'doctor', NULL, 'Psiquiatría', 'profile-icon.png'),
(5, 'Dr. Martín', 'Vargas', 'ac1c8d64fd23ae5a7eac5b7f7ffee1fa', 'mvargas@healthnow.com', '111-999-888', '1987-06-15', 'doctor', NULL, 'Medicina Interna', 'profile-icon.png'),
(6, 'Dra. Isabella', 'Silva', '161ebd7d45089b3446ee4e0d86dbcf92', 'isilva@healthnow.com', '555-777-222', '1993-08-09', 'doctor', NULL, 'Oftalmología', 'profile-icon.png'),
(7, 'Dr. Andrés', 'Castro', '1ffc66a8aed4d82417ee075f2194d002', 'acastro@healthnow.com', '333-666-444', '1984-02-23', 'doctor', NULL, 'Ortopedia y Traumatología', 'profile-icon.png'),
(8, 'Dra. Laura', 'Gómez', 'ef749ff9a048bad0dd80807fc49e1c0d', 'lgomez@healthnow.com', '789-456-123', '1991-11-17', 'doctor', NULL, 'Endocrinología', 'profile-icon.png'),
(9, 'Dr. Leonardo', 'Ramírez', 'b41cb62ec6767f2e41f9df7a2d161515', 'lramirez@healthnow.com', '678-901-234', '1986-10-03', 'doctor', NULL, 'Medicina Deportiva', 'profile-icon.png'),
(10, 'Dra. Valentina', 'Fuentes', '6eea9b7ef19179a06954edd0f6c05ceb', 'vfuentes@healthnow.com', '555-888-777', '1998-12-14', 'doctor', NULL, 'Medicina Familiar', 'profile-icon.png'),
(11, 'Dr. Eduardo', 'Soto', 'e807f1fcf82d132f9bb018ca6738a19f', 'esoto@healthnow.com', '222-444-666', '1975-08-25', 'doctor', NULL, 'Urología', 'profile-icon.png'),
(12, 'Dra. Carolina', 'Paredes', 'fd56e30c3b64536939f3c4879f2b6946', 'cparedes@healthnow.com', '999-333-555', '1997-09-06', 'doctor', NULL, 'Oncología', 'profile-icon.png'),
(13, 'Dra. Valeria', 'Montenegro', '3488e28acfe4abe097e1f4d501d4b49a', 'vmontenegro@healthnow.com', '111-222-333', '1985-03-10', 'doctor', NULL, 'Cardiología', 'profile-icon.png'),
(14, 'Dr. Sebastián', 'Herrera', '2af9b1ba42dc5eb01743e6b3759b6e4b', 'sherrera@healthnow.com', '444-555-666', '1992-09-22', 'doctor', NULL, 'Neurología', '1683908348832.jpg'),
(15, 'Dra. Sofía', 'Rodríguez', '25f9e794323b453885f5181f1b624d0b', 'srodriguez@healthnow.com', '777-888-999', '1978-05-04', 'doctor', NULL, 'Dermatología', 'profile-icon.png'),
(16, 'Otoniel', 'Pérez Velarde', '741391797dbc13b8ffb0c97f83e3fb86', 'otonielcuentas@gmail.com', '123-456-789', '2000-10-07', 'patient', NULL, NULL, '1684946401026.jpg'),
(18, 'Test', 'User', '644941af66ff4422f702f85da26788ed', 'testuser@gmail.com', '123-456-789', '1997-05-20', 'patient', NULL, NULL, 'profile-icon.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `uc_DoctorId_Date` (`DoctorId`,`Date`);

--
-- Indices de la tabla `consultations`
--
ALTER TABLE `consultations`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Email` (`Email`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `appointments`
--
ALTER TABLE `appointments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consultations`
--
ALTER TABLE `consultations`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;