const mysql = require('mysql');

class ConnectionProvider {
    constructor() {
        this.connection;
    }

    getConnection(isTestEnv) {
        require('dotenv').config();

        if (!this.connection)
        {
            let database = "consultas_medicas";
            database += (isTestEnv || process.env.NODE_ENV == 'test') ? "_test" : "_dev";

            this.connection = mysql.createPool({
                host     : process.env.DB_TFG_HOST || 'localhost',
                user     : process.env.DB_TFG_USER || 'root',
                password : process.env.DB_TFG_PASS || '',
                database : process.env.DB_TFG_NAME || database,
                port: process.env.DB_TFG_PORT || 3306,
                multipleStatements: true
            });
        }

        return this.connection;
    }
}

module.exports = ConnectionProvider;