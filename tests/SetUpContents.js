class SetUpContents {

    static TruncateTable(sqlconnection, tableToDelete)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = "TRUNCATE " + tableToDelete;
            sqlconnection.query(sql, function(error)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve();
                }
            });
        });
    }

    static CountRepositoryContent(sqlconnection, repoTable)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `SELECT * FROM ${repoTable}`;
            sqlconnection.query(sql, function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results.length);
                }
            });
        });
    }

    static GivenAnyUser(sqlconnection, userInfo)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `INSERT INTO users (Name, Lastname, Password, Email, Telephone, Birthdate, Type, Speciality) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
            sqlconnection.query(sql, [userInfo.Name, userInfo.Lastname, userInfo.Password, userInfo.Email, userInfo.Telephone, new Date(userInfo.Birthdate), userInfo.Type || "patient", userInfo.Speciality || ""], function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results.length);
                }
            });
        });
    }

    static GetAnyUser(sqlconnection, userId)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `SELECT * FROM users WHERE Id=?`;
            sqlconnection.query(sql, userId, function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results);
                }
            });
        });
    }

    static GivenAnyConsultation(sqlconnection, appointmentInfo)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `INSERT INTO consultations (PatientId, DoctorId, Title, Messages, IsClosed) VALUES (?, ?, ?, ?, ?)`;
            sqlconnection.query(sql, [appointmentInfo.PatientId, appointmentInfo.DoctorId, appointmentInfo.Title, appointmentInfo.Messages, appointmentInfo.IsClosed || 0], function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results.length);
                }
            });
        });
    }

    static GetAnyConsultation(sqlconnection, consultationId)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `SELECT * FROM consultations WHERE Id=?`;
            sqlconnection.query(sql, consultationId, function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results);
                }
            });
        });
    }

    static GivenAnyAppointment(sqlconnection, appointmentInfo)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `INSERT INTO appointments (PatientId, DoctorId, Type, Date) VALUES (?, ?, ?, ?)`;
            sqlconnection.query(sql, [appointmentInfo.PatientId, appointmentInfo.DoctorId, appointmentInfo.Type, appointmentInfo.Date], function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results.length);
                }
            });
        });
    }

    static GetAnyAppointment(sqlconnection, appointmentId)
    {
        return new Promise(function(resolve, reject)
        {
            let sql = `SELECT * FROM appointments WHERE Id=?`;
            sqlconnection.query(sql, appointmentId, function(error, results)
            {
                if (error)
                {
                    reject(error);
                }
                else
                { 
                    resolve(results);
                }
            });
        });
    }
}

module.exports = SetUpContents;