const SetUpContents = require('../SetUpContents');
const ConnectionProvider = require("../../Infrastructure/ConnectionProvider");
const md5 = require('md5');
const request = require('supertest');
const app = require('../../app');
const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "appointments");
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetAppointmentsQueryHandlerShould.test', () => {
    test('get all appointments for specified patient ordered by Id Asc', async () => {
        var aDate = new Date();
        var anotherDate = new Date("3023-05-21");
        var appoitnmentInfo = {
            PatientId: "2",
            DoctorId: "1",
            Type: "phone",
            Date: aDate
        }
        var anotherAppointmentInfo = {
            PatientId: "2",
            DoctorId: "1",
            Type: "phone",
            Date: anotherDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherAppointmentInfo);

        var response = await request(app).get("/api/appointments?userId=2");

        var results = JSON.parse(response.text);
        expect(results.length).toBe(2);
        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[1].Id).toBe(2);
        expect(results[1].Type).toBe("phone");
    });

    test('get doctors full name for patients appointments', async () => {
        var aDate = new Date();
        var appoitnmentInfo = {
            PatientId: "2",
            DoctorId: "1",
            Type: "phone",
            Date: aDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);

        var response = await request(app).get("/api/appointments?userId=2");

        var results = JSON.parse(response.text);
        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[0].OtherFullName).toBe("aName aLastName");
    });

    test('get patients full name for doctors appointments', async () => {
        var aDate = new Date();
        var appoitnmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: aDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "patient"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);

        var response = await request(app).get("/api/appointments?userId=2&isDoctor=true");

        var results = JSON.parse(response.text);
        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[0].OtherFullName).toBe("aName aLastName");
    });

    test('get only appointments from the current date to the next week date for specified doctor ordered by Id', async () => {
        var currentDate = new Date("3023-05-21");
        var aDate = new Date("3023-05-21 18:00:00");
        var anotherDate = new Date("3023-05-20 12:00:00");
        var anotherOneDate = new Date("3023-05-29 12:00:00");
        var appoitnmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: aDate
        }
        var anotherAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherDate
        }
        var anotherOneAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherOneDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "patient"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherAppointmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherOneAppointmentInfo);

        var response = await request(app).get(`/api/appointments?userId=2&isDoctor=true&currentDate=${currentDate}`);

        var results = JSON.parse(response.text);
        expect(results.length).toBe(1);
        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[0].Date).toBe(aDate.toISOString());
    });

    test('create a new appointment', async () => {
        var newAppointmentPayload = {
            patientId: "1",
            doctorId: "2",
            type: "aType",
            date: new Date()
        }

        await request(app).post("/api/appointments").send(newAppointmentPayload);

        var results = await SetUpContents.GetAnyAppointment(sqlconnection, "1");
        expect(results[0].PatientId).toBe(1);
        expect(results[0].DoctorId).toBe(2);
        expect(results[0].Type).toBe("aType");
    });
});

