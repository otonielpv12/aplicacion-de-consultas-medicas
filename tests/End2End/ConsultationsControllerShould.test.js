const SetUpContents = require('../SetUpContents');
const ConnectionProvider = require("../../Infrastructure/ConnectionProvider");
const md5 = require('md5');
const request = require('supertest');
const app = require('../../app');
const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "consultations");
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('CreateNewConsultationCommandHandlerShould', () => {
    test('create a new consultation', async () => {
        var aPatientId = "1";
        var aDoctorId = "2";
        var aTitle = "aTitle";
        var aMessage = "aMessage";
        var consultationInfo = {
            patientId: aPatientId,
            doctorId: aDoctorId,
            title: aTitle,
            message: aMessage
        }

        await request(app).post("/api/consultations").send(consultationInfo);

        var results = await SetUpContents.GetAnyConsultation(sqlconnection, "1");
        expect(results[0].PatientId).toBe(1);
        expect(results[0].DoctorId).toBe(2);
        expect(results[0].Title).toBe(aTitle);
        var messages = JSON.parse(results[0].Messages);
        expect(messages[0].id).toBe(0);
        expect(messages[0].userId).toBe(1);
        expect(messages[0].message).toBe(aMessage);
    });

    test('insert a new message on a consultation', async () => {
        var messages = [{
            id: 0,
            userId: 1,
            date: new Date(),
            message: "aMessage"
        }];
        var consultationInfo = {
            PatientId: "1",
            DoctorId: "2",
            Title: "aTitle",
            Messages: JSON.stringify(messages)
        }
        var newMessagePayload = {
            userId: 2,
            message: "anotherMessage"
        }
        await SetUpContents.GivenAnyConsultation(sqlconnection, consultationInfo);

        await request(app).put("/api/consultations/1").send(newMessagePayload);

        var results = await SetUpContents.GetAnyConsultation(sqlconnection, "1");
        expect(results[0].PatientId).toBe(1);
        expect(results[0].DoctorId).toBe(2);
        expect(results[0].Title).toBe("aTitle");
        var messages = JSON.parse(results[0].Messages);
        expect(messages.length).toBe(2);
        expect(messages[0].id).toBe(0);
        expect(messages[0].userId).toBe(1);
        expect(messages[0].message).toBe("aMessage");
        expect(messages[1].id).toBe(1);
        expect(messages[1].userId).toBe(2);
        expect(messages[1].message).toBe("anotherMessage");
    });

    test('get all consultations for specified user ordered by IsClosed and Id with doctor name', async () => {
        var messages = [{
            id: 0,
            userId: 2,
            date: new Date(),
            message: "aMessage"
        }];
        var consultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "aTitle",
            Messages: JSON.stringify(messages)
        }
        var anotherConsultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "anotherTitle",
            Messages: JSON.stringify(messages)
        }
        var aClosedConsultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "anotherClosedTitle",
            Messages: JSON.stringify(messages),
            IsClosed: 1
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyConsultation(sqlconnection, consultationInfo);
        await SetUpContents.GivenAnyConsultation(sqlconnection, anotherConsultationInfo);
        await SetUpContents.GivenAnyConsultation(sqlconnection, aClosedConsultationInfo);

        var response = await request(app).get("/api/consultations?userId=2");

        var results = JSON.parse(response.text);
        expect(results[0].PatientId).toBe(2);
        expect(results[0].Title).toBe("anotherTitle");
        expect(results[1].Title).toBe("aTitle");
        expect(results[2].Title).toBe("anotherClosedTitle");
        expect(results[0].IsClosed).toBe(0);
        expect(results[1].IsClosed).toBe(0);
        expect(results[2].IsClosed).toBe(1);
        expect(results[0].ReceptorsFullName).toBe("aName aLastName");
    });

    test('close a consultation', async () => {
        var messages = [{
            id: 0,
            userId: 2,
            date: new Date(),
            message: "aMessage"
        }];
        var consultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "aTitle",
            Messages: JSON.stringify(messages)
        }
        await SetUpContents.GivenAnyConsultation(sqlconnection, consultationInfo);

        await request(app).put("/api/consultations/1/close");

        var results = await SetUpContents.GetAnyConsultation(sqlconnection, "1");
        expect(results[0].PatientId).toBe(2);
        expect(results[0].Title).toBe("aTitle");
        expect(results[0].IsClosed).toBe(1);
    });
});

