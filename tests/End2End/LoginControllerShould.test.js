const SetUpContents = require('../SetUpContents');
const ConnectionProvider = require("../../Infrastructure/ConnectionProvider");
const md5 = require('md5');
const request = require('supertest')
const app = require('../../app');
const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('LoginControllerShould', () => {
    test('get id and type for a user with email and password', async () => {
        var aPassword = "1234";
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: md5(aPassword),
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        var aLoginInfo = {
            password: aPassword,
            email: "anEmail"
        }

        var response = await request(app).post("/api/login").send(aLoginInfo);
        
        var results = JSON.parse(response.text);

        expect(results.userId).toBe(1);
        expect(results.type).toBe("patient");
    });
});

