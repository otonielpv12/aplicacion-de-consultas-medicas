const SetUpContents = require('../SetUpContents');
const ConnectionProvider = require("../../Infrastructure/ConnectionProvider");
const md5 = require('md5');
const request = require('supertest')
const app = require('../../app');
const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
    await SetUpContents.TruncateTable(sqlconnection, "appointments");
});

describe('DoctorsControllerShould', () => {
    test('get all doctors', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "aSpeciality"
            
        }
        var anotherUserInfo = {
            Name: "anotherName",
            Lastname: "anotherLastName",
            Password: aPassword,
            Email: "anotherEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "anotherSpeciality"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyUser(sqlconnection, anotherUserInfo);

        var response = await request(app).get("/api/doctors");

        var results = JSON.parse(response.text);
        expect(results[0].DoctorsFullName).toBe("aName aLastName");
        expect(results[0].Email).toBe("anEmail");
        expect(results[0].Telephone).toBe("123-456-789");
        expect(results[0].Speciality).toBe("aSpeciality");
        expect(results[1].DoctorsFullName).toBe("anotherName anotherLastName");
        expect(results[1].Email).toBe("anotherEmail");
        expect(results[1].Telephone).toBe("123-456-789");
        expect(results[1].Speciality).toBe("anotherSpeciality");
    });

    test('get all doctors with limit', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "aSpeciality"
            
        }
        var anotherUserInfo = {
            Name: "anotherName",
            Lastname: "anotherLastName",
            Password: aPassword,
            Email: "anotherEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "anotherSpeciality"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyUser(sqlconnection, anotherUserInfo);

        var response = await request(app).get("/api/doctors?limit=1");

        var results = JSON.parse(response.text);
        expect(results.length).toBe(1);
        expect(results[0].DoctorsFullName).toBe("aName aLastName");
        expect(results[0].Email).toBe("anEmail");
        expect(results[0].Telephone).toBe("123-456-789");
        expect(results[0].Speciality).toBe("aSpeciality");
    });

    test('get all doctors specialities ordered alphabetically', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "aSpeciality"
            
        }
        var anotherUserInfo = {
            Name: "anotherName",
            Lastname: "anotherLastName",
            Password: aPassword,
            Email: "anotherEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "anotherSpeciality"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyUser(sqlconnection, anotherUserInfo);

        var response = await request(app).get("/api/doctors/specialities");

        var results = JSON.parse(response.text);
        expect(results.length).toBe(2);
        expect(results[0].Speciality).toBe("anotherSpeciality");
        expect(results[0].Count).toBe(1);
        expect(results[1].Speciality).toBe("aSpeciality");
        expect(results[1].Count).toBe(1);
        
    });

    test('get all appointments dates from current date for a specified doctor', async () => {
        var currentDate = new Date("3023-05-21");
        var aDate = new Date("3023-05-21 18:00:00");
        var anotherDate = new Date("3023-05-20 12:00:00");
        var anotherOneDate = new Date("3023-05-29 12:00:00");
        var appoitnmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: aDate
        }
        var anotherAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherDate
        }
        var anotherOneAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherOneDate
        }
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherAppointmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherOneAppointmentInfo);

        var response = await request(app).get(`/api/doctors/2/appointments?currentDate=${currentDate}`);

        var results = JSON.parse(response.text);
        expect(results.length).toBe(2);
        expect(results[0].Date).toBe(aDate.toISOString());
        expect(results[1].Date).toBe(anotherOneDate.toISOString());
    });
});