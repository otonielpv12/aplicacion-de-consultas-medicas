const SetUpContents = require('../SetUpContents');
const ConnectionProvider = require("../../Infrastructure/ConnectionProvider");
const md5 = require('md5');
const request = require('supertest')
const app = require('../../app');
const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('UsersControllerShould', () => {
    test('create new user', async () => {
        var aPassword = "1234";
        var aUserInfo = {
            name: "aName",
            lastname: "aLastName",
            password: md5(aPassword),
            email: "anEmail",
            telephone: "123-456-789",
            birthdate: "2000-01-01"
        }

       await request(app).post("/api/users").send(aUserInfo);
        
       var results = await SetUpContents.CountRepositoryContent(sqlconnection, "users");
       expect(results).toBe(1);
    });

    test('get user info if it exists', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);

        var response = await request(app).get("/api/users/1");
        
        var results = JSON.parse(response.text);
        expect(results.Name).toBe("aName");
        expect(results.Lastname).toBe("aLastName");
        expect(results.Email).toBe("anEmail");
        expect(results.Telephone).toBe("123-456-789");
        expect(results.Birthdate).toBe("2000-01-01");
    });

    test('throw 404 when getting users and it does not exists', async () => {

        var response = await request(app).get("/api/users/1");
        
        expect(response.statusCode).toBe(404);
    });

    test('throw 403 when user is not at least 18 years old', async () => {
        var aPassword = "1234";
        var aUserInfo = {
            name: "aName",
            lastname: "aLastName",
            password: md5(aPassword),
            email: "anEmail",
            telephone: "123-456-789",
            birthdate: "2023-01-01"
        }

       var response = await request(app).post("/api/users").send(aUserInfo);
        
       expect(response.statusCode).toBe(403);
    });

    test('update user password', async () => {
        var aUserId = "1";
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
            
        }
        var updatePasswordInfo = {
            currentPassword: "1234",
            newPassword: "4567"
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);

        await request(app).post("/api/users/1").send(updatePasswordInfo);

        var results = await SetUpContents.GetAnyUser(sqlconnection, aUserId);
        expect(results[0].Password).toBe(md5("4567"));
    });

    test('throw 409 if current password does not exists', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
            
        }
        var updatePasswordInfo = {
            currentPassword: "1111",
            newPassword: "4567"
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);

        var response = await request(app).post("/api/users/1").send(updatePasswordInfo);

        expect(response.statusCode).toBe(409);
    });
});

