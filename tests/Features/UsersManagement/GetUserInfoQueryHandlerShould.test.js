const GetUserInfoQueryHandler = require('../../../Features/UsersManagement/Queries/GetUserInfoQueryHandler');
const SetUpContents = require('../../SetUpContents');
const md5 = require('md5');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const GetUserInfoQuery = require('../../../Features/UsersManagement/Queries/GetUserInfoQuery');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetUserInfoQueryHandlerShould', () => {
    test('Get user info if it exists', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
            
        }
        var aUserId = "1";
        var userQuery = new GetUserInfoQuery(aUserId);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);

        var results = await GetUserInfoQueryHandler.Handle(sqlconnection, userQuery);

        expect(results.Name).toBe("aName");
        expect(results.Lastname).toBe("aLastName");
        expect(results.Email).toBe("anEmail");
        expect(results.Telephone).toBe("123-456-789");
        expect(results.Birthdate).toBe("2000-01-01");
    });

    test('throw an error when user does not exist', async () => {
        var aUserId = "1";
        var userQuery = new GetUserInfoQuery(aUserId);

        const action = async () => { await GetUserInfoQueryHandler.Handle(sqlconnection, userQuery); };

        await expect(action()).rejects.toThrow(Error("El usuario no está registrado"));
    });
});

