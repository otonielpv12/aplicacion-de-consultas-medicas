const CreateUserCommandHandler = require('../../../Features/UsersManagement/Commands/CreateUserCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const CreateUserCommand = require('../../../Features/UsersManagement/Commands/CreateUserCommand');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('CreateUserCommandHandlerShould', () => {
    test('Create new user', async () => {
        var aPassword = "1234";
        var aBirthdate = new Date('2000-01-01');
        var userCommand = new CreateUserCommand("aName", "aLastName" ,aPassword, "anEmail", "123-456-789", aBirthdate);

        CreateUserCommandHandler.Handle(sqlconnection, userCommand);

        var results = await SetUpContents.CountRepositoryContent(sqlconnection, "users");

        expect(results).toBe(1);
    });

    test('throw an error if user has not at least 18 years old', async () => {
        var aPassword = "1234";
        var aBirthdate = '2023-01-01';
        var userCommand = new CreateUserCommand("aName", "aLastName" ,aPassword, "anEmail", "123-456-789", aBirthdate);

        const action = async () => { await CreateUserCommandHandler.Handle(sqlconnection, userCommand); };
        
        await expect(action()).rejects.toThrow(Error("Debes ser mayor de edad para registrarte"));
    });
});

