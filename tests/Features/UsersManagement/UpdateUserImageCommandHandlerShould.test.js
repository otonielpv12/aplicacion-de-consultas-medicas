const UpdateUserImageCommandHandler = require('../../../Features/UsersManagement/Commands/UpdateUserImageCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const UpdateUserImageCommand = require('../../../Features/UsersManagement/Commands/UpdateUserImageCommand');
const md5 = require('md5');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('UpdateUserImageCommandHandlerShould', () => {
    test('update user image name', async () => {
        var aUserId = "1";
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        var userCommand = new UpdateUserImageCommand(aUserId, "image.png");

        await UpdateUserImageCommandHandler.Handle(sqlconnection, userCommand);

        var results = await SetUpContents.GetAnyUser(sqlconnection, aUserId);
        expect(results[0].Image).toBe("image.png");
    });
});

