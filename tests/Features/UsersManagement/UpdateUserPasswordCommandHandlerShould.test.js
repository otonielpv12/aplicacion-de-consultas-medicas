const UpdateUserPasswordCommandHandler = require('../../../Features/UsersManagement/Commands/UpdateUserPasswordCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const UpdateUserPasswordCommand = require('../../../Features/UsersManagement/Commands/UpdateUserPasswordCommand');
const md5 = require('md5');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('UpdateUserPasswordCommandHandlerShould', () => {
    test('update user password', async () => {
        var aUserId = "1";
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        var userCommand = new UpdateUserPasswordCommand(aUserId, "1234", "4567");

        await UpdateUserPasswordCommandHandler.Handle(sqlconnection, userCommand);

        var results = await SetUpContents.GetAnyUser(sqlconnection, aUserId);
        expect(results[0].Password).toBe(md5("4567"));
    });

    test('throw an error if current password does not exists', async () => {
        var aUserId = "1";
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
            
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        var userCommand = new UpdateUserPasswordCommand(aUserId, "1111", "4567");

        const action = async () => { await UpdateUserPasswordCommandHandler.Handle(sqlconnection, userCommand) };

        await expect(action()).rejects.toThrow(Error("Contraseña actual incorrecta"));
    });
});

