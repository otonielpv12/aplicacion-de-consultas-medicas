const InsertConsultationMessageCommandHandler = require('../../../Features/ConsultationsManagement/Commands/InsertConsultationMessageCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const InsertConsultationMessageCommand = require('../../../Features/ConsultationsManagement/Commands/InsertConsultationMessageCommand');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "consultations");
});

describe('InsertConsultationMessageCommandHandlerShould', () => {
    test('insert a new message on a consultation', async () => {
        var messages = [{
            id: 0,
            userId: 1,
            date: new Date(),
            message: "aMessage"
        }];
        var consultationInfo = {
            PatientId: "1",
            DoctorId: "2",
            Title: "aTitle",
            Messages: JSON.stringify(messages)
        }
        var consultationCommand = new InsertConsultationMessageCommand(1, 2 , "anotherMessage");
        await SetUpContents.GivenAnyConsultation(sqlconnection, consultationInfo);

        await InsertConsultationMessageCommandHandler.Handle(sqlconnection, consultationCommand);

        var results = await SetUpContents.GetAnyConsultation(sqlconnection, "1");
        expect(results[0].PatientId).toBe(1);
        expect(results[0].DoctorId).toBe(2);
        expect(results[0].Title).toBe("aTitle");
        var messages = JSON.parse(results[0].Messages);
        expect(messages.length).toBe(2);
        expect(messages[0].id).toBe(0);
        expect(messages[0].userId).toBe(1);
        expect(messages[0].message).toBe("aMessage");
        expect(messages[1].id).toBe(1);
        expect(messages[1].userId).toBe(2);
        expect(messages[1].message).toBe("anotherMessage");
    });
});

