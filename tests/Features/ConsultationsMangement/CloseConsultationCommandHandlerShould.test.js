const CloseConsultationCommandHandler = require('../../../Features/ConsultationsManagement/Commands/CloseConsultationCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const CloseConsultationCommand = require('../../../Features/ConsultationsManagement/Commands/CloseConsultationCommand');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "consultations");
});

describe('CloseConsultationCommandHandlerShould.test', () => {
    test('close a consultation', async () => {
        var messages = [{
            id: 0,
            userId: 2,
            date: new Date(),
            message: "aMessage"
        }];
        var consultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "aTitle",
            Messages: JSON.stringify(messages)
        }
        var consultationCommand= new CloseConsultationCommand(1);
        await SetUpContents.GivenAnyConsultation(sqlconnection, consultationInfo);

        await CloseConsultationCommandHandler.Handle(sqlconnection, consultationCommand);

        var results = await SetUpContents.GetAnyConsultation(sqlconnection, "1");
        expect(results[0].PatientId).toBe(2);
        expect(results[0].Title).toBe("aTitle");
        expect(results[0].IsClosed).toBe(1);
    });
});

