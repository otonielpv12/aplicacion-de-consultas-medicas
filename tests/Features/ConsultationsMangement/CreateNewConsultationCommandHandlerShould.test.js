const CreateNewConsultationCommandHandler = require('../../../Features/ConsultationsManagement/Commands/CreateNewConsultationCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const CreateNewConsultationCommand = require('../../../Features/ConsultationsManagement/Commands/CreateNewConsultationCommand');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "consultations");
});

describe('CreateNewConsultationCommandHandlerShould', () => {
    test('create a new consultation', async () => {
        var aPatientId = "1";
        var aDoctorId = "2";
        var aTitle = "aTitle";
        var aMessage = "aMessage";
        var consultationCommand = new CreateNewConsultationCommand(aPatientId, aDoctorId ,aTitle, aMessage);

        CreateNewConsultationCommandHandler.Handle(sqlconnection, consultationCommand);

        var results = await SetUpContents.GetAnyConsultation(sqlconnection, "1");

        expect(results[0].PatientId).toBe(1);
        expect(results[0].DoctorId).toBe(2);
        expect(results[0].Title).toBe(aTitle);
        var messages = JSON.parse(results[0].Messages);
        expect(messages[0].id).toBe(0);
        expect(messages[0].userId).toBe(1);
        expect(messages[0].message).toBe(aMessage);
    });
});

