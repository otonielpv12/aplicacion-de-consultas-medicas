const GetConsultationsQueryHandler = require('../../../Features/ConsultationsManagement/Queries/GetConsultationsQueryHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const GetConsultationsQuery = require('../../../Features/ConsultationsManagement/Queries/GetConsultationsQuery');
const md5 = require("md5");

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "consultations");
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetConsultationsQueryHandlerShould.test', () => {
    test('get all consultations for specified user ordered by IsClosed and Id with doctor name', async () => {
        var messages = [{
            id: 0,
            userId: 2,
            date: new Date(),
            message: "aMessage"
        }];
        var consultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "aTitle",
            Messages: JSON.stringify(messages)
        }
        var anotherConsultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "anotherTitle",
            Messages: JSON.stringify(messages)
        }
        var aClosedConsultationInfo = {
            PatientId: "2",
            DoctorId: "1",
            Title: "anotherClosedTitle",
            Messages: JSON.stringify(messages),
            IsClosed: 1
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor"
            
        }
        var consultationQuery = new GetConsultationsQuery(2);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyConsultation(sqlconnection, consultationInfo);
        await SetUpContents.GivenAnyConsultation(sqlconnection, anotherConsultationInfo);
        await SetUpContents.GivenAnyConsultation(sqlconnection, aClosedConsultationInfo);

        var results = await GetConsultationsQueryHandler.Handle(sqlconnection, consultationQuery);

        expect(results[0].PatientId).toBe(2);
        expect(results[0].Title).toBe("anotherTitle");
        expect(results[1].Title).toBe("aTitle");
        expect(results[2].Title).toBe("anotherClosedTitle");
        expect(results[0].IsClosed).toBe(0);
        expect(results[1].IsClosed).toBe(0);
        expect(results[2].IsClosed).toBe(1);
        expect(results[0].ReceptorsFullName).toBe("aName aLastName");
    });
});

