const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const md5 = require('md5');
const UserLoginCommand = require('../../../Features/Authentication/Commands/UserLoginCommand');
const UserLoginCommandHandler = require('../../../Features/Authentication/Commands/UserLoginCommandHandler');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('UserLoginCommandHandlerShould', () => {
    test('get id and type for a user with email and password', async () => {
        var aPassword = md5("1234");
        var userLoginCommand = new UserLoginCommand("1234", "anEmail");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01"
        }
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);

        var results = await UserLoginCommandHandler.Handle(sqlconnection, userLoginCommand);

        expect(results.userId).toBe(1);
        expect(results.type).toBe("patient");
    });
});

