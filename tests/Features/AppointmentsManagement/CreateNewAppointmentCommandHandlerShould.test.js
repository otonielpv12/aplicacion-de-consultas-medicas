const CreateNewAppointmentCommandHandler = require('../../../Features/AppointmentsManagement/Commands/CreateNewAppointmentCommandHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const CreateNewAppointmentCommand = require('../../../Features/AppointmentsManagement/Commands/CreateNewAppointmentCommand');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "appointments");
});

describe('CreateNewAppointmentCommandHandlerShould', () => {
    test('create a new appointment', async () => {
        var aPatientId = "1";
        var aDoctorId = "2";
        var aType = "aType";
        var aDate = new Date();
        var appointmentCommand = new CreateNewAppointmentCommand(aPatientId, aDoctorId ,aType, aDate);

        CreateNewAppointmentCommandHandler.Handle(sqlconnection, appointmentCommand);

        var results = await SetUpContents.GetAnyAppointment(sqlconnection, "1");
        expect(results[0].PatientId).toBe(1);
        expect(results[0].DoctorId).toBe(2);
        expect(results[0].Type).toBe(aType);
    });
});

