const GetAppointmentsQueryHandler = require('../../../Features/AppointmentsManagement/Queries/GetAppointmentsQueryHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const GetAppointmentsQuery = require('../../../Features/AppointmentsManagement/Queries/GetAppointmentsQuery');
const md5 = require("md5");

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "appointments");
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetAppointmentsQueryHandlerShould.test', () => {
    test('get all appointments for specified patient ordered by Id', async () => {
        var aDate = new Date();
        var anotherDate = new Date("3023-05-21");
        var appoitnmentInfo = {
            PatientId: "2",
            DoctorId: "1",
            Type: "phone",
            Date: aDate
        }
        var anotherAppointmentInfo = {
            PatientId: "2",
            DoctorId: "1",
            Type: "phone",
            Date: anotherDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor"
            
        }
        var appointmentsQuery = new GetAppointmentsQuery(2);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherAppointmentInfo);

        var results = await GetAppointmentsQueryHandler.Handle(sqlconnection, appointmentsQuery);

        expect(results.length).toBe(2);
        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[1].Id).toBe(2);
        expect(results[1].Type).toBe("phone");
    });

    test('get doctors full name for patients appointments', async () => {
        var aDate = new Date();
        var appoitnmentInfo = {
            PatientId: "2",
            DoctorId: "1",
            Type: "phone",
            Date: aDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor"
            
        }
        var appointmentsQuery = new GetAppointmentsQuery(2);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);

        var results = await GetAppointmentsQueryHandler.Handle(sqlconnection, appointmentsQuery);

        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[0].OtherFullName).toBe("aName aLastName");
    });

    test('get patients full name for doctors appointments', async () => {
        var aDate = new Date();
        var appoitnmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: aDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "patient"
            
        }
        var appointmentsQuery = new GetAppointmentsQuery(2, true);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);

        var results = await GetAppointmentsQueryHandler.Handle(sqlconnection, appointmentsQuery);

        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[0].OtherFullName).toBe("aName aLastName");
    });

    test('get only appointments from the current date to the next week date for specified doctor ordered by Id', async () => {
        var currentDate = new Date("3023-05-21");
        var aDate = new Date("3023-05-21 18:00:00");
        var anotherDate = new Date("3023-05-20 12:00:00");
        var anotherOneDate = new Date("3023-05-29 12:00:00");
        var appoitnmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: aDate
        }
        var anotherAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherDate
        }
        var anotherOneAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherOneDate
        }
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "patient"
            
        }
        var appointmentsQuery = new GetAppointmentsQuery(2, true, currentDate);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherAppointmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherOneAppointmentInfo);

        var results = await GetAppointmentsQueryHandler.Handle(sqlconnection, appointmentsQuery);

        expect(results.length).toBe(1);
        expect(results[0].Id).toBe(1);
        expect(results[0].Type).toBe("phone");
        expect(results[0].Date.toISOString()).toBe(aDate.toISOString());
    });
});

