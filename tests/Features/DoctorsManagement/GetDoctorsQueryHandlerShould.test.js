const GetDoctorsQueryHandler = require('../../../Features/DoctorsManagement/Queries/GetDoctorsQueryHandler');
const SetUpContents = require('../../SetUpContents');
const md5 = require('md5');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const GetDoctorsQuery = require('../../../Features/DoctorsManagement/Queries/GetDoctorsQuery');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetDoctorsQueryHandlerShould', () => {
    test('get all doctors', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "aSpeciality"
            
        }
        var anotherUserInfo = {
            Name: "anotherName",
            Lastname: "anotherLastName",
            Password: aPassword,
            Email: "anotherEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "anotherSpeciality"
            
        }
        var doctorsQuery = new GetDoctorsQuery();
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyUser(sqlconnection, anotherUserInfo);

        var results = await GetDoctorsQueryHandler.Handle(sqlconnection, doctorsQuery);

        expect(results[0].DoctorsFullName).toBe("aName aLastName");
        expect(results[0].Email).toBe("anEmail");
        expect(results[0].Telephone).toBe("123-456-789");
        expect(results[0].Speciality).toBe("aSpeciality");
        expect(results[1].DoctorsFullName).toBe("anotherName anotherLastName");
        expect(results[1].Email).toBe("anotherEmail");
        expect(results[1].Telephone).toBe("123-456-789");
        expect(results[1].Speciality).toBe("anotherSpeciality");
    });

    test('get all doctors wiht limit', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "aSpeciality"
            
        }
        var anotherUserInfo = {
            Name: "anotherName",
            Lastname: "anotherLastName",
            Password: aPassword,
            Email: "anotherEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "anotherSpeciality"
            
        }
        var doctorsQuery = new GetDoctorsQuery(1);
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyUser(sqlconnection, anotherUserInfo);

        var results = await GetDoctorsQueryHandler.Handle(sqlconnection, doctorsQuery);

        expect(results.length).toBe(1);
        expect(results[0].DoctorsFullName).toBe("aName aLastName");
        expect(results[0].Email).toBe("anEmail");
        expect(results[0].Telephone).toBe("123-456-789");
        expect(results[0].Speciality).toBe("aSpeciality");
    });
});

