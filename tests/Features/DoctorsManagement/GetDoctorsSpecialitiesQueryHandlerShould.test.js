const GetDoctorsSpecialitiesQueryHandler = require('../../../Features/DoctorsManagement/Queries/GetDoctorsSpecialitiesQueryHandler');
const SetUpContents = require('../../SetUpContents');
const md5 = require('md5');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const GetDoctorsSpecialitiesQuery = require('../../../Features/DoctorsManagement/Queries/GetDoctorsSpecialitiesQuery');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetDoctorsSpecialitiesQueryHandlerShould', () => {
    test('get all doctors specialities ordered alphabetically', async () => {
        var aPassword = md5("1234");
        var aUserInfo = {
            Name: "aName",
            Lastname: "aLastName",
            Password: aPassword,
            Email: "anEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "aSpeciality"
            
        }
        var anotherUserInfo = {
            Name: "anotherName",
            Lastname: "anotherLastName",
            Password: aPassword,
            Email: "anotherEmail",
            Telephone: "123-456-789",
            Birthdate: "2000-01-01",
            Type: "doctor",
            Speciality: "anotherSpeciality"
            
        }
        var doctorsQuery = new GetDoctorsSpecialitiesQuery();
        await SetUpContents.GivenAnyUser(sqlconnection, aUserInfo);
        await SetUpContents.GivenAnyUser(sqlconnection, anotherUserInfo);

        var results = await GetDoctorsSpecialitiesQueryHandler.Handle(sqlconnection, doctorsQuery);

        expect(results.length).toBe(2);
        expect(results[0].Speciality).toBe("anotherSpeciality");
        expect(results[0].Count).toBe(1);
        expect(results[1].Speciality).toBe("aSpeciality");
        expect(results[1].Count).toBe(1);
        
    });
});

