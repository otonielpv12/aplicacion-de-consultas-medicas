const GetDoctorAppointmentsDatesQueryHandler = require('../../../Features/DoctorsManagement/Queries/GetDoctorAppointmentsDatesQueryHandler');
const SetUpContents = require('../../SetUpContents');
const ConnectionProvider = require("../../../Infrastructure/ConnectionProvider");
const GetDoctorAppointmentsDatesQuery = require('../../../Features/DoctorsManagement/Queries/GetDoctorAppointmentsDatesQuery');

const IS_TEST_ENV = true;
const connectionProvider = new ConnectionProvider();
const sqlconnection = connectionProvider.getConnection(IS_TEST_ENV);

beforeEach(async() => {
    await SetUpContents.TruncateTable(sqlconnection, "appointments");
    await SetUpContents.TruncateTable(sqlconnection, "users");
});

describe('GetDoctorAppointmentsDatesQueryHandlerShould.test', () => {
    test('get all appointments dates from current date for a specified doctor', async () => {
        var currentDate = new Date("3023-05-21");
        var aDate = new Date("3023-05-21 18:00:00");
        var anotherDate = new Date("3023-05-20 12:00:00");
        var anotherOneDate = new Date("3023-05-29 12:00:00");
        var appoitnmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: aDate
        }
        var anotherAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherDate
        }
        var anotherOneAppointmentInfo = {
            PatientId: "1",
            DoctorId: "2",
            Type: "phone",
            Date: anotherOneDate
        }
        var appointmentsQuery = new GetDoctorAppointmentsDatesQuery(2, currentDate);
        await SetUpContents.GivenAnyAppointment(sqlconnection, appoitnmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherAppointmentInfo);
        await SetUpContents.GivenAnyAppointment(sqlconnection, anotherOneAppointmentInfo);

        var results = await GetDoctorAppointmentsDatesQueryHandler.Handle(sqlconnection, appointmentsQuery);

        expect(results.length).toBe(2);
        expect(results[0].Date.toISOString()).toBe(aDate.toISOString());
        expect(results[1].Date.toISOString()).toBe(anotherOneDate.toISOString());
    });
});

