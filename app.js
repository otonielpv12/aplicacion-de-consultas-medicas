require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
let app = express();

let port = process.env.PORT || 8181;

const usersController = require('./Controllers/usersController');
const loginController = require('./Controllers/LoginController');
const doctorsController = require('./Controllers/DoctorsController');
const consultationsController = require('./Controllers/ConsultationsController');
const appointmentsController = require('./Controllers/AppointmentsController');

var path;

if (process.env.ENVIRONMENT == "Production") {
    path = process.env.STATIC_FILES_PATH || __dirname + '/client-app/dist/';
} else {
    path = __dirname + '/client-app/public/'
}

app.use(express.static(path));
app.use(morgan('dev'))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/users", usersController);
app.use("/api/login", loginController);
app.use("/api/doctors", doctorsController);
app.use("/api/consultations", consultationsController);
app.use("/api/appointments", appointmentsController);


if (!process.env.IS_TEST_ENV) {
    app.listen(port, function () {
        console.log("Running on PORT: " + port);
    });
}

module.exports = app;